import React, { useState } from 'react';
import { evaluate } from 'mathjs';
import './Calculator.css';

const Calculator = () => {
    const [result, setResult] = useState('');

    const insert = (num) => {
        setResult(result + num);
    };

    const clean = () => {
        setResult('');
    };

    const back = () => {
        setResult(result.slice(0, -1));
    };

    const calculate = () => {
        try {
            setResult(evaluate(result) + '');
        } catch (err) {
            setResult('Error');
        }
    };

    return (
        <div className="background">
            <div className="calculator">
                <h1>Calculadora</h1>
                <p id="results">{result}</p>
                <table>
                    <tbody>
                    <tr>
                        <td><button className="button" onClick={() => clean()}>C</button></td>
                        <td><button className="button" onClick={() => back()}>&lt;</button></td>
                        <td><button className="button" onClick={() => insert('/')}>/</button></td>
                        <td><button className="button" onClick={() => insert('*')}>*</button></td>
                    </tr>
                    <tr>
                        <td><button className="button" onClick={() => insert('7')}>7</button></td>
                        <td><button className="button" onClick={() => insert('8')}>8</button></td>
                        <td><button className="button" onClick={() => insert('9')}>9</button></td>
                        <td><button className="button" onClick={() => insert('-')}>-</button></td>
                    </tr>
                    <tr>
                        <td><button className="button" onClick={() => insert('4')}>4</button></td>
                        <td><button className="button" onClick={() => insert('5')}>5</button></td>
                        <td><button className="button" onClick={() => insert('6')}>6</button></td>
                        <td><button className="button" onClick={() => insert('+')}>+</button></td>
                    </tr>
                    <tr>
                        <td><button className="button" onClick={() => insert('1')}>1</button></td>
                        <td><button className="button" onClick={() => insert('2')}>2</button></td>
                        <td><button className="button" onClick={() => insert('3')}>3</button></td>
                        <td rowSpan="2"><button className="button" style={{height: '106px'}} onClick={() => calculate()}>=</button></td>
                    </tr>
                    <tr>
                        <td colSpan="2"><button className="button" style={{width: '106px'}} onClick={() => insert('0')}>0</button></td>
                        <td><button className="button" onClick={() => insert('.')}>.</button></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default Calculator;